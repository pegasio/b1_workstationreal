# Rendu TP1

## Self-footprinting

### Host Os

* 
* 

```
C:\Users\33778\Documents\b1_workstation\b1_workstationreal>systeminfo

Nom de l’hôte:                              LAPTOP-EQFL9EMU
Nom du système d’exploitation:              Microsoft Windows 10 Professionnel
Version du système:                         10.0.18363 N/A version 18363
[...]
Processeur(s):                              1 processeur(s) installé(s).
                                            [01] : Intel64 Family 6 Model 158 Stepping 10 GenuineIntel ~2592 MHz
[...]
Mémoire physique totale:                    16 221 Mo
Mémoire physique disponible:                9 172 Mo
Mémoire virtuelle : taille maximale:        18 653 Mo
Mémoire virtuelle : disponible:             9 371 Mo
Mémoire virtuelle : en cours d’utilisation: 9 282 Mo
[...]
```

*
*

### Devices

```
C:\Users\33778\Documents\b1_workstation\b1_workstationreal>wmic cpu
AddressWidth  Architecture  AssetTag                Availability  Caption                                 Characteristics  ConfigManagerErrorCode  ConfigManagerUserConfig  CpuStatus  CreationClassName  CurrentClockSpeed  CurrentVoltage  DataWidth  Description                             DeviceID  ErrorCleared  ErrorDescription  ExtClock  Family  InstallDate  L2CacheSize  L2CacheSpeed  L3CacheSize  L3CacheSpeed  LastErrorCode  Level  LoadPercentage  Manufacturer  MaxClockSpeed  Name                                      NumberOfCores  NumberOfEnabledCore  NumberOfLogicalProcessors  OtherFamilyDescription  PartNumber              PNPDeviceID  PowerManagementCapabilities  PowerManagementSupported  ProcessorId       ProcessorType  Revision  Role  SecondLevelAddressTranslationExtensions  SerialNumber            SocketDesignation  Status  StatusInfo  Stepping  SystemCreationClassName  SystemName       ThreadCount  UniqueId  UpgradeMethod  Version  VirtualizationFirmwareEnabled  VMMonitorModeExtensions  VoltageCaps
64            9             To Be Filled By O.E.M.  3             Intel64 Family 6 Model 158 Stepping 10  252                                                               1          Win32_Processor    2592               9               64         Intel64 Family 6 Model 158 Stepping 10  CPU0                                      100       198                  1536                       12288        0                            6      5               GenuineIntel  2592           Intel(R) Core(TM) i7-9750H CPU @ 2.60GHz  6              6                    12                                                 To Be Filled By O.E.M.                                            FALSE                     BFEBFBFF000906EA  3                        CPU   TRUE                                     To Be Filled By O.E.M.  U3E1               OK      3                     Win32_ComputerSystem     LAPTOP-EQFL9EMU  12                     52                      TRUE                           TRUE

```

* *explication nom du processeur* : i7-9750H 
* i7 -> avant denier niveau de la marque intel pour des performances de jeu de haut niveau.
* 9 -> generation du processeur.
* 750 -> Le numéro de processeur, qui vient après le numéro de génération, sert à différencier les caractéristiques au sein d'une famille de processeurs, notamment la fréquence d'horloge de base, la fréquence maximale, la taille du cache, le nombre de cœurs et de threads, la prise en charge de la mémoire, etc. Ces chiffres ne se recoupent pas avec les différentes familles de processeurs.
* H -> Les CPU avec un suffixe H appartiennent à la série H, une série de processeurs mobiles puissants pour PC portables.
*
*

```
C:\Users\33778\Documents\b1_workstation\b1_workstationreal>wmic MemoryChip
Attributes  BankLabel  Capacity     Caption           ConfiguredClockSpeed  ConfiguredVoltage  CreationClassName     DataWidth  Description       DeviceLocator   FormFactor  HotSwappable  InstallDate  InterleaveDataDepth  InterleavePosition  Manufacturer  MaxVoltage  MemoryType  MinVoltage  Model  Name              OtherIdentifyingInfo  PartNumber        PositionInRow  PoweredOn  Removable  Replaceable  SerialNumber  SKU  SMBIOSMemoryType  Speed  Status  Tag                TotalWidth  TypeDetail  Version
2           BANK 0     17179869184  Mémoire physique  2667                  1200               Win32_PhysicalMemory  64         Mémoire physique  ChannelA-DIMM0  12                                     1                    1                   Kingston      0           0           0                  Mémoire physique                        ACR26D4S9D8ME-16                                                    6B696CF0           26                2667           Physical Memory 0  64          128

```

*
*

```
C:\Users\33778\Documents\b1_workstation\b1_workstationreal>wmic path win32_VideoController get name
Name
NVIDIA GeForce GTX 1650
Intel(R) UHD Graphics 630
```

*
*

* n'arruve pas aux trackpad/touchpad

*
*

```
C:\Users\33778\Documents\b1_workstation\b1_workstationreal>wmic bios
BiosCharacteristics                                                          BIOSVersion                                         BuildNumber  Caption  CodeSet  CurrentLanguage  Description  EmbeddedControllerMajorVersion  EmbeddedControllerMinorVersion  IdentificationCode  InstallableLanguages  InstallDate  LanguageEdition  ListOfLanguages  Manufacturer  Name   OtherTargetOS  PrimaryBIOS  ReleaseDate                SerialNumber            SMBIOSBIOSVersion  SMBIOSMajorVersion  SMBIOSMinorVersion  SMBIOSPresent  SoftwareElementID  SoftwareElementState  Status  SystemBiosMajorVersion  SystemBiosMinorVersion  TargetOperatingSystem  Version
{7, 11, 12, 15, 16, 19, 20, 21, 22, 23, 24, 25, 27, 30, 32, 33, 40, 42, 43}  {"ACRSYS - 2", "V1.29", "INSYDE Corp. - 58104129"}               V1.29                              V1.29        1                               27                                                                                                                       Insyde Corp.  V1.29                 TRUE         20200409000000.000000+000  NXC4QEF0020211B9843400  V1.29              3                   0                   TRUE           V1.29              3                     OK      1                       29                      0                      ACRSYS - 2

```

*
*

```
DISKPART> sel disk 0

Le disque 0 est maintenant le disque sélectionné.

DISKPART> detail disk

WDC WD10SPZX-21Z10T0
ID du disque : {CBB85078-E7F3-442C-BFE6-00253E61CE0A}
Type : RAID
[...]

  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
  Volume 0     D   Data         NTFS   Partition    931 G   Sain

DISKPART> sel disk 1

Le disque 1 est maintenant le disque sélectionné.

DISKPART> detail disk

HFM256GDJTNG-8310A
ID du disque : {DA2DB4C9-6BE0-48DA-9E72-0C4B620FBE5B}
Type : RAID
[...]

  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
  Volume 1     C   Acer         NTFS   Partition    237 G   Sain       Démarrag
  Volume 2         ESP          FAT32  Partition    100 M   Sain       Système
```

*
*

NTFS -> Le système de fichiers NTFS utilise un système basé sur une structure appelée MFT , permettant de contenir des informations détaillées sur les fichiers. Ce système permet ainsi l'utilisation de noms longs, mais,contrairement au système FAT32, il est sensible à la casse, c'est-à-dire qu'il est capable de différencier des noms en majuscules de noms en minuscules.
FAT32 ->FAT32 est un système de fichiers inventé par Microsoft. Utilisant des adresses sur 28 bits, il permet de constituer des unités d’allocation de taille réduite sur des disques de taille importante

*
*

### Users

*
*

```
PS C:\Users\33778> net users

comptes d’utilisateurs de \\LAPTOP-EQFL9EMU

-------------------------------------------------------------------------------
33778                    Administrateur           DefaultAccount
Invité                   WDAGUtilityAccount
La commande s’est terminée correctement.
```

*
*

### Processus

*
*

```
PS C:\Users\33778> tasklist

Nom de l’image                 PID Nom de la sessio Numéro de s Utilisation
========================= ======== ================ =========== ============
System Idle Process              0 Services                   0         8 Ko
System                           4 Services                   0     5 404 Ko
Registry                       144 Services                   0    32 548 Ko
smss.exe                       628 Services                   0       516 Ko
csrss.exe                      800 Services                   0     2 616 Ko
wininit.exe                    900 Services                   0     3 684 Ko
services.exe                   972 Services                   0     8 316 Ko
lsass.exe                      984 Services                   0    16 672 Ko
svchost.exe                    112 Services                   0     1 128 Ko
svchost.exe                   1044 Services                   0    29 656 Ko
fontdrvhost.exe               1052 Services                   0     1 636 Ko
WUDFHost.exe                  1140 Services                   0     8 836 Ko
svchost.exe                   1224 Services                   0    14 480 Ko
svchost.exe                   1280 Services                   0     6 244 Ko
svchost.exe                   1468 Services                   0     6 680 Ko
svchost.exe                   1476 Services                   0     8 168 Ko
svchost.exe                   1540 Services                   0     4 780 Ko
svchost.exe                   1696 Services                   0     4 656 Ko
WUDFHost.exe                  1716 Services                   0     2 916 Ko
svchost.exe                   1800 Services                   0     7 552 Ko
svchost.exe                   1852 Services                   0    13 704 Ko
svchost.exe                   1860 Services                   0     4 600 Ko
NVDisplay.Container.exe       1920 Services                   0    12 496 Ko
svchost.exe                   1976 Services                   0    14 580 Ko
svchost.exe                   1996 Services                   0     8 304 Ko
svchost.exe                    968 Services                   0    10 084 Ko
svchost.exe                   1080 Services                   0     1 768 Ko
svchost.exe                   1240 Services                   0     6 712 Ko
svchost.exe                   1276 Services                   0     4 296 Ko
svchost.exe                   1952 Services                   0     6 720 Ko
svchost.exe                   2160 Services                   0     3 992 Ko
Memory Compression            2192 Services                   0   170 584 Ko
svchost.exe                   2268 Services                   0     5 732 Ko
svchost.exe                   2296 Services                   0    10 752 Ko
igfxCUIService.exe            2372 Services                   0     3 928 Ko
svchost.exe                   2424 Services                   0     9 976 Ko
svchost.exe                   2452 Services                   0     5 344 Ko
svchost.exe                   2512 Services                   0     3 904 Ko
svchost.exe                   2520 Services                   0     3 708 Ko
svchost.exe                   2692 Services                   0     5 944 Ko
svchost.exe                   2836 Services                   0     6 216 Ko
svchost.exe                   2908 Services                   0    10 744 Ko
svchost.exe                   2768 Services                   0     5 988 Ko
svchost.exe                   2632 Services                   0     2 976 Ko
svchost.exe                   3204 Services                   0    15 648 Ko
svchost.exe                   3264 Services                   0    11 036 Ko
wlanext.exe                   3340 Services                   0     2 320 Ko
conhost.exe                   3360 Services                   0     1 756 Ko
spoolsv.exe                   3568 Services                   0     7 900 Ko
svchost.exe                   3608 Services                   0    11 108 Ko
svchost.exe                   3632 Services                   0    16 040 Ko
svchost.exe                   3720 Services                   0     2 328 Ko
svchost.exe                   3880 Services                   0     3 580 Ko
svchost.exe                   3952 Services                   0    16 528 Ko
svchost.exe                   3964 Services                   0     3 052 Ko
IntelCpHDCPSvc.exe            2728 Services                   0     2 448 Ko
svchost.exe                   2708 Services                   0    29 892 Ko
ACCSvc.exe                    3224 Services                   0     2 972 Ko
svchost.exe                   3924 Services                   0     2 612 Ko
svchost.exe                   1328 Services                   0     3 428 Ko
svchost.exe                   4104 Services                   0     6 076 Ko
ADCCSvc.exe                   4112 Services                   0    16 440 Ko
svchost.exe                   4124 Services                   0     9 464 Ko
RstMwService.exe              4132 Services                   0     2 676 Ko
svchost.exe                   4144 Services                   0    12 260 Ko
LMS.exe                       4152 Services                   0     9 328 Ko
IntelAudioService.exe         4160 Services                   0    19 472 Ko
svchost.exe                   4176 Services                   0    18 580 Ko
svchost.exe                   4184 Services                   0    25 632 Ko
svchost.exe                   4192 Services                   0     1 532 Ko
NortonSecurity.exe            4200 Services                   0    29 048 Ko
svchost.exe                   4208 Services                   0     2 028 Ko
RtkAudUService64.exe          4216 Services                   0     3 928 Ko
MsMpEng.exe                   4228 Services                   0   244 976 Ko
OfficeClickToRun.exe          4236 Services                   0    44 992 Ko
nsWscSvc.exe                  4272 Services                   0     5 460 Ko
IntelCpHeciSvc.exe            4620 Services                   0     2 404 Ko
dasHost.exe                   4640 Services                   0     1 356 Ko
svchost.exe                   4888 Services                   0     6 436 Ko
svchost.exe                   5032 Services                   0     1 416 Ko
jhi_service.exe               5184 Services                   0     1 716 Ko
WmiPrvSE.exe                  5280 Services                   0     9 436 Ko
svchost.exe                   5604 Services                   0     3 604 Ko
svchost.exe                   5620 Services                   0     4 272 Ko
svchost.exe                   5736 Services                   0     9 740 Ko
NisSrv.exe                    6428 Services                   0     9 648 Ko
svchost.exe                   6488 Services                   0     2 460 Ko
dllhost.exe                   6660 Services                   0     6 828 Ko
PresentationFontCache.exe     6176 Services                   0     4 004 Ko
svchost.exe                   2444 Services                   0    13 932 Ko
svchost.exe                   7220 Services                   0     3 716 Ko
svchost.exe                   7644 Services                   0    17 892 Ko
svchost.exe                   8100 Services                   0     4 488 Ko
GoogleCrashHandler.exe        8380 Services                   0     1 384 Ko
GoogleCrashHandler64.exe      8404 Services                   0     1 336 Ko
SearchIndexer.exe             8636 Services                   0    53 720 Ko
svchost.exe                   8784 Services                   0     6 872 Ko
CDPSvc.exe                    9360 Services                   0     9 432 Ko
svchost.exe                  10372 Services                   0     8 476 Ko
SecurityHealthService.exe    11708 Services                   0    11 976 Ko
QASvc.exe                    12704 Services                   0     6 108 Ko
svchost.exe                  11492 Services                   0    12 328 Ko
svchost.exe                  11120 Services                   0    13 092 Ko
svchost.exe                  10756 Services                   0     6 416 Ko
IAStorDataMgrSvc.exe          8268 Services                   0    77 948 Ko
SgrmBroker.exe                6896 Services                   0     4 120 Ko
svchost.exe                  12552 Services                   0     7 760 Ko
svchost.exe                  14200 Services                   0    12 612 Ko
svchost.exe                  14396 Services                   0     7 664 Ko
svchost.exe                   2780 Services                   0     5 160 Ko
svchost.exe                  11132 Services                   0    15 500 Ko
svchost.exe                   4648 Services                   0     4 112 Ko
svchost.exe                  13328 Services                   0     4 208 Ko
WmiPrvSE.exe                  6520 Services                   0    19 004 Ko
svchost.exe                   9244 Services                   0     7 764 Ko
csrss.exe                     7060 Console                    4     6 096 Ko
winlogon.exe                  8444 Console                    4    10 140 Ko
dwm.exe                      18036 Console                    4   107 072 Ko
fontdrvhost.exe              15964 Console                    4     5 216 Ko
NVDisplay.Container.exe       2016 Console                    4    38 460 Ko
sihost.exe                   10064 Console                    4    28 848 Ko
svchost.exe                   5688 Console                    4    32 792 Ko
igfxEM.exe                   13040 Console                    4    27 656 Ko
svchost.exe                  10000 Console                    4    36 020 Ko
explorer.exe                 18960 Console                    4   173 356 Ko
taskhostw.exe                14580 Console                    4    19 104 Ko
svchost.exe                  13372 Console                    4    22 956 Ko
StartMenuExperienceHost.e    14512 Console                    4    78 496 Ko
RuntimeBroker.exe              916 Console                    4    26 076 Ko
SearchUI.exe                  3808 Console                    4   245 340 Ko
RuntimeBroker.exe            15960 Console                    4    37 172 Ko
SettingSyncHost.exe          12868 Console                    4    13 652 Ko
ctfmon.exe                   12424 Console                    4    15 000 Ko
CDPAgent.exe                 15044 Console                    4     1 960 Ko
CDPAdminAgent.exe             8356 Console                    4    11 864 Ko
RtkAudUService64.exe         13516 Console                    4    10 112 Ko
unsecapp.exe                  5424 Console                    4     9 892 Ko
RuntimeBroker.exe            12948 Console                    4    21 532 Ko
YourPhoneServer.exe          16660 Console                    4    17 292 Ko
svchost.exe                  13624 Console                    4    23 456 Ko
SecurityHealthSystray.exe    17884 Console                    4     8 764 Ko
RtkAudUService64.exe         13820 Console                    4    11 544 Ko
vgtray.exe                   15236 Console                    4     6 936 Ko
OneDrive.exe                 18416 Console                    4    61 068 Ko
ApplicationFrameHost.exe      8740 Console                    4    38 796 Ko
RuntimeBroker.exe            17032 Console                    4    24 924 Ko
WinStore.App.exe              7564 Console                    4       544 Ko
RuntimeBroker.exe            12736 Console                    4    24 732 Ko
Discord.exe                   2920 Console                    4    76 208 Ko
Discord.exe                   1880 Console                    4   112 500 Ko
Discord.exe                   1068 Console                    4    27 968 Ko
steam.exe                     7528 Console                    4    72 916 Ko
Discord.exe                   4544 Console                    4    13 640 Ko
Discord.exe                   8088 Console                    4   279 980 Ko
NortonSecurity.exe           16016 Console                    4    10 484 Ko
steamwebhelper.exe           15944 Console                    4    53 212 Ko
SteamService.exe             16640 Services                   0    11 212 Ko
QAAgent.exe                   6624 Console                    4     9 212 Ko
EpicGamesLauncher.exe         5112 Console                    4   121 896 Ko
steamwebhelper.exe           12572 Console                    4    14 220 Ko
steamwebhelper.exe           10932 Console                    4    57 832 Ko
QAAdminAgent.exe              6592 Console                    4    14 324 Ko
steamwebhelper.exe            7864 Console                    4    28 036 Ko
Discord.exe                   1440 Console                    4    18 496 Ko
igfxext.exe                   9036 Console                    4     9 840 Ko
Teams.exe                    18008 Console                    4   151 024 Ko
UnrealCEFSubProcess.exe      15464 Console                    4    35 220 Ko
Teams.exe                     3620 Console                    4    77 048 Ko
Teams.exe                    14988 Console                    4    56 960 Ko
Teams.exe                     1748 Console                    4   425 524 Ko
QALockHandler.exe            16164 Console                    4    10 480 Ko
unsecapp.exe                 10620 Console                    4     9 696 Ko
Teams.exe                    19180 Console                    4   109 764 Ko
Teams.exe                    17688 Console                    4   145 964 Ko
steamwebhelper.exe            3896 Console                    4    44 260 Ko
steamwebhelper.exe           15612 Console                    4   108 996 Ko
steamwebhelper.exe            2980 Console                    4    81 420 Ko
IAStorIcon.exe               16888 Console                    4    41 288 Ko
HxTsr.exe                     1596 Console                    4    18 732 Ko
commsapps.exe                17000 Console                    4       524 Ko
HostAppServiceUpdater.exe    19352 Console                    4     8 216 Ko
SystemSettings.exe            7044 Console                    4       648 Ko
UserOOBEBroker.exe           18224 Console                    4     8 376 Ko
ePowerButton_NB.exe          18932 Console                    4    11 156 Ko
ACCStd.exe                   15984 Console                    4    22 664 Ko
WindowsInternal.Composabl    12468 Console                    4    48 348 Ko
RuntimeBroker.exe             6204 Console                    4     5 980 Ko
AcerRegistrationBackGroun     5356 Console                    4    52 164 Ko
dllhost.exe                  14640 Console                    4     6 880 Ko
chrome.exe                   13044 Console                    4   320 888 Ko
chrome.exe                    9664 Console                    4     6 652 Ko
chrome.exe                   16728 Console                    4   225 800 Ko
chrome.exe                   14840 Console                    4    36 952 Ko
chrome.exe                    6180 Console                    4    14 860 Ko
chrome.exe                    6560 Console                    4   106 432 Ko
chrome.exe                   18952 Console                    4   116 812 Ko
dllhost.exe                   8568 Console                    4    14 264 Ko
powershell.exe                9420 Console                    4    75 064 Ko
conhost.exe                  16484 Console                    4    17 428 Ko
chrome.exe                   16536 Console                    4   106 652 Ko
chrome.exe                    7080 Console                    4   142 332 Ko
chrome.exe                    6696 Console                    4    49 284 Ko
chrome.exe                    1588 Console                    4   105 116 Ko
chrome.exe                   17152 Console                    4    97 140 Ko
chrome.exe                    8032 Console                    4    21 816 Ko
LockApp.exe                  12660 Console                    4    62 708 Ko
RuntimeBroker.exe             6924 Console                    4    32 520 Ko
RuntimeBroker.exe            14344 Console                    4    12 084 Ko
svchost.exe                   9784 Services                   0    10 660 Ko
svchost.exe                    896 Services                   0     5 532 Ko
YourPhone.exe                 5312 Console                    4    26 104 Ko
svchost.exe                    684 Services                   0     8 152 Ko
ShellExperienceHost.exe       2236 Console                    4    51 716 Ko
RuntimeBroker.exe            12700 Console                    4    16 480 Ko
Teams.exe                     9164 Console                    4    34 608 Ko
svchost.exe                  16988 Services                   0    19 764 Ko
TrustedInstaller.exe          4072 Services                   0     6 836 Ko
TiWorker.exe                 18608 Services                   0    27 856 Ko
tasklist.exe                  5564 Console                    4    10 092 Ko
```

*
*

### Network

*
*

```
PS C:\Users\33778> Get-NetAdapter

Name                      InterfaceDescription                    ifIndex Status       MacAddress             LinkSpeed
----                      --------------------                    ------- ------       ----------             ---------
VirtualBox Host-Only N... VirtualBox Host-Only Ethernet Adapter        19 Up           0A-00-27-00-00-13         1 Gbps
Ethernet                  Realtek Gaming GbE Family Controller         17 Disconnected 08-97-98-97-C1-31          0 bps
Wi-Fi                     Intel(R) Wi-Fi 6 AX200 160MHz                10 Up           14-F6-D8-43-8F-00     144.4 Mbps

```

*
*

```
PS C:\Users\33778> Get-NetTCPConnection

LocalAddress                        LocalPort RemoteAddress                       RemotePort State       AppliedSetting OwningProcess
------------                        --------- -------------                       ---------- -----       -------------- -------------
::                                  55661     ::                                  0          Bound                      4200
::                                  55132     ::                                  0          Bound                      7564
::                                  55131     ::                                  0          Bound                      7564
::                                  55130     ::                                  0          Bound                      7564
::                                  49670     ::                                  0          Listen                     972
::1                                 49669     ::                                  0          Listen                     5184
::                                  49668     ::                                  0          Listen                     3568
::                                  49667     ::                                  0          Listen                     2296
::                                  49666     ::                                  0          Listen                     1976
::                                  49665     ::                                  0          Listen                     900
::                                  49664     ::                                  0          Listen                     984
::                                  7680      ::                                  0          Listen                     11120
::                                  445       ::                                  0          Listen                     4
::                                  135       ::                                  0          Listen                     1224
0.0.0.0                             55982     0.0.0.0                             0          Bound                      5688
0.0.0.0                             55979     0.0.0.0                             0          Bound                      14840
0.0.0.0                             55978     0.0.0.0                             0          Bound                      14840
0.0.0.0                             55977     0.0.0.0                             0          Bound                      14840
0.0.0.0                             55976     0.0.0.0                             0          Bound                      1068
0.0.0.0                             55971     0.0.0.0                             0          Bound                      4200
0.0.0.0                             55970     0.0.0.0                             0          Bound                      5112
0.0.0.0                             55916     0.0.0.0                             0          Bound                      14840
0.0.0.0                             55881     0.0.0.0                             0          Bound                      18008
0.0.0.0                             55875     0.0.0.0                             0          Bound                      14840
0.0.0.0                             55851     0.0.0.0                             0          Bound                      1068
0.0.0.0                             55850     0.0.0.0                             0          Bound                      4200
0.0.0.0                             55846     0.0.0.0                             0          Bound                      5112
0.0.0.0                             55814     0.0.0.0                             0          Bound                      1068
0.0.0.0                             55813     0.0.0.0                             0          Bound                      7528
0.0.0.0                             55798     0.0.0.0                             0          Bound                      4176
0.0.0.0                             55797     0.0.0.0                             0          Bound                      18416
0.0.0.0                             55796     0.0.0.0                             0          Bound                      19180
0.0.0.0                             55319     0.0.0.0                             0          Bound                      7044
0.0.0.0                             55135     0.0.0.0                             0          Bound                      7564
0.0.0.0                             55134     0.0.0.0                             0          Bound                      7564
10.33.2.172                         55984     52.114.77.34                        443        TimeWait                   0
10.33.2.172                         55982     20.44.232.74                        443        Established Internet       5688
10.33.2.172                         55981     52.114.77.34                        443        TimeWait                   0
10.33.2.172                         55979     52.196.10.8                         443        Established Internet       14840
10.33.2.172                         55978     3.218.125.188                       443        Established Internet       14840
10.33.2.172                         55977     3.218.125.188                       443        Established Internet       14840
10.33.2.172                         55976     162.159.130.233                     443        Established Internet       1068
10.33.2.172                         55971     13.88.181.35                        443        Established Internet       4200
10.33.2.172                         55970     52.45.40.11                         443        CloseWait   Internet       5112
10.33.2.172                         55916     18.182.129.64                       443        Established Internet       14840
10.33.2.172                         55881     52.114.92.54                        443        Established Internet       18008
10.33.2.172                         55875     64.233.166.188                      5228       Established Internet       14840
10.33.2.172                         55851     35.186.224.47                       443        Established Internet       1068
10.33.2.172                         55850     13.68.168.63                        443        Established Internet       4200
10.33.2.172                         55846     3.218.184.250                       443        Established Internet       5112
10.33.2.172                         55814     162.159.134.234                     443        Established Internet       1068
10.33.2.172                         55813     155.133.248.39                      27036      Established Internet       7528
10.33.2.172                         55798     40.67.251.132                       443        Established Internet       4176
10.33.2.172                         55797     40.67.251.132                       443        Established Internet       18416
10.33.2.172                         55796     52.113.199.228                      443        Established Internet       19180
0.0.0.0                             49670     0.0.0.0                             0          Listen                     972
0.0.0.0                             49668     0.0.0.0                             0          Listen                     3568
0.0.0.0                             49667     0.0.0.0                             0          Listen                     2296
0.0.0.0                             49666     0.0.0.0                             0          Listen                     1976
0.0.0.0                             49665     0.0.0.0                             0          Listen                     900
0.0.0.0                             49664     0.0.0.0                             0          Listen                     984
127.0.0.1                           27060     0.0.0.0                             0          Listen                     7528
0.0.0.0                             27036     0.0.0.0                             0          Listen                     7528
127.0.0.1                           6463      0.0.0.0                             0          Listen                     8088
0.0.0.0                             5040      0.0.0.0                             0          Listen                     7644
192.168.56.1                        139       0.0.0.0                             0          Listen                     4
10.33.2.172                         139       0.0.0.0                             0          Listen                     4
0.0.0.0                             135       0.0.0.0                             0          Listen                     1224
```

*
*

## Scripting

*
*

```
PS C:\Windows\system32> $nom_machine = $env:computername                                                                                                                                                                                     PS C:\Windows\system32> echo $nom_machine                                                                                                                                                                                                    LAPTOP-EQFL9EMU
```

*
*

```
PS C:\Windows\system32> Get-NetIPAddress -InterfaceAlias 'Wi-Fi' -AddressFamily 'IPv4'                                                                                                                                                       

IPAddress         : 10.33.2.172
InterfaceIndex    : 10
InterfaceAlias    : Wi-Fi
AddressFamily     : IPv4
Type              : Unicast
PrefixLength      : 22
PrefixOrigin      : Dhcp
SuffixOrigin      : Dhcp
AddressState      : Preferred
ValidLifetime     : 00:31:29
PreferredLifetime : 00:31:29
SkipAsSource      : False
PolicyStore       : ActiveStore



PS C:\Windows\system32> $yo = Get-NetIPAddress -InterfaceAlias 'Wi-Fi' -AddressFamily 'IPv4'                                                                                                                                                 PS C:\Windows\system32> Write-Output $yo                                                                                                                                                                                                     

IPAddress         : 10.33.2.172
InterfaceIndex    : 10
InterfaceAlias    : Wi-Fi
AddressFamily     : IPv4
Type              : Unicast
PrefixLength      : 22
PrefixOrigin      : Dhcp
SuffixOrigin      : Dhcp
AddressState      : Preferred
ValidLifetime     : 00:31:00
PreferredLifetime : 00:31:00
SkipAsSource      : False
PolicyStore       : ActiveStore



PS C:\Windows\system32> Write-Output $yo.IPAddress                                                                                                                                                                                           10.33.2.172
```

*
*

## Gestion de softs

* L'interet par rapport au telechargements direct sur internet et que l'on a accés au fichier en cours de telechargements ainsi on peut voir et etre conscient de ce qui se download. Si c'est nous qui avons la mains mise sur la globalité des telechargements il y a moins de risque de telecharger un mauvais fichier et ainsi on securise mieux notre machine.






